package com.example.coinwashmanager2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoinWashManager2Application {

    public static void main(String[] args) {
        SpringApplication.run(CoinWashManager2Application.class, args);
    }

}
