package com.example.coinwashmanager2.model;

import com.example.coinwashmanager2.entity.Machine;
import com.example.coinwashmanager2.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MachineItem {
    @ApiModelProperty(notes = "기계 시퀀스")
    private Long id;
    @ApiModelProperty(notes = "기계 풀네임")
    private String machineFullName;
    @ApiModelProperty(notes = "기계 구매날짜")
    private LocalDate datePurchase;

    private MachineItem(MachineItemBuilder machineItemBuilder) {
        this.id = machineItemBuilder.id;
        this.machineFullName = machineItemBuilder.machineFullName;
        this.datePurchase = machineItemBuilder.datePurchase;
    }

    public static class MachineItemBuilder implements CommonModelBuilder<MachineItem> {

        private final Long id;

        private final String machineFullName;

        private final LocalDate datePurchase;

        public MachineItemBuilder(Machine machine) {
            this.id = machine.getId();
            this.machineFullName = "[" + machine.getMachineType().getName() + "]" + machine.getMachineName();
            this.datePurchase = machine.getDatePurchase();
        }
        @Override
        public MachineItem build() {
            return new MachineItem(this);
        }
    }
}
