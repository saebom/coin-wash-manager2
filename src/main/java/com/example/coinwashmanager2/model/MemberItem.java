package com.example.coinwashmanager2.model;

import com.example.coinwashmanager2.entity.Member;
import com.example.coinwashmanager2.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {

    @ApiModelProperty(notes = "회원 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "회원 이름")
    private String memberName;

    @ApiModelProperty(notes = "회원 전화번호")
    private String memberPhone;

    @ApiModelProperty(notes = "회원 생년월일")
    private LocalDate birthday;

    @ApiModelProperty(notes = "회원 활성화여부")
    private String isEnable;

    @ApiModelProperty(notes = "회원 가입일")
    private LocalDateTime dateJoin;

    @ApiModelProperty(notes = "회원 탈퇴일")
    private LocalDateTime dateWithdrawal;

    private MemberItem(MemberItemBuilder builder) {
        this.id = builder.id;
        this.memberName = builder.memberName;
        this.birthday = builder.birthday;
        this.isEnable = builder.isEnable;  //오류
        this.dateJoin = builder.dateJoin;
        this.dateWithdrawal = builder.dateWithdrawal;
    }

    public static class MemberItemBuilder implements CommonModelBuilder<MemberItem> {

        private final Long id;
        private final String memberName;
        private final LocalDate birthday;
        private final  String isEnable;
        private final LocalDateTime dateJoin;

        private final LocalDateTime dateWithdrawal;

        public MemberItemBuilder(Member member) {
            this.id = member.getId();
            this.memberName = member.getMemberName();
            this.birthday = member.getBirthday();
            this.isEnable = member.getIsEnable() ? "예" : "아니오";
            this.dateJoin = member.getDateJoin();
            this.dateWithdrawal = member.getDateWithdrawal();

        }
        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}
