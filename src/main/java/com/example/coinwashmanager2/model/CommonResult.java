package com.example.coinwashmanager2.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CommonResult {
    private Boolean isSuccess;
    private Integer code;
    private String msg;
}
