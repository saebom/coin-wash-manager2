package com.example.coinwashmanager2.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MemberJoinRequest {
    @ApiModelProperty(notes = "고객 이름(2~20자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String memberName;

    @ApiModelProperty(notes = "고객 전화번호 (2~20자)", required = true)
    @NotNull
    @Length(min = 13, max = 20)
    private String memberPhone;

    @ApiModelProperty(notes = "고객 생년월일", required = true)
    @NotNull
    private LocalDate birthday;
}
