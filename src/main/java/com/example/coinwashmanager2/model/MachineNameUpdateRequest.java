package com.example.coinwashmanager2.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MachineNameUpdateRequest {
    @ApiModelProperty(notes = "기계 이름(1~20자)",required = true)
    private String machineName;
}
