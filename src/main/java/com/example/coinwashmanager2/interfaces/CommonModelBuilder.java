package com.example.coinwashmanager2.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
