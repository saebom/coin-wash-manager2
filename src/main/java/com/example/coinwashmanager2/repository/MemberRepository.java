package com.example.coinwashmanager2.repository;

import com.example.coinwashmanager2.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MemberRepository extends JpaRepository<Member, Long> {
    List<Member> findAllByIsEnableOrderByIdDesc(Boolean isEnable);
}
