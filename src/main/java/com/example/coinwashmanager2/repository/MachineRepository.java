package com.example.coinwashmanager2.repository;

import com.example.coinwashmanager2.entity.Machine;
import com.example.coinwashmanager2.enums.MachineType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MachineRepository extends JpaRepository<Machine, Long> {
    List<Machine> findAllByMachineTypeOrderByIdDesc(MachineType machineType);
}
