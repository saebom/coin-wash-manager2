package com.example.coinwashmanager2.repository;

import com.example.coinwashmanager2.entity.UsageDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface UsageDetailsRepository extends JpaRepository<UsageDetails, Long> {
    List<UsageDetails> findAllByDateUsageGreaterThanEqualAndDateUsageLessThanEqualOrderByIdDesc(LocalDateTime dateStart, LocalDateTime dateEnd);

}
