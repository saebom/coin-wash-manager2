package com.example.coinwashmanager2.service;

import com.example.coinwashmanager2.entity.Member;
import com.example.coinwashmanager2.exception.CMissingDataException;
import com.example.coinwashmanager2.exception.CNoMemberDataException;
import com.example.coinwashmanager2.model.ListResult;
import com.example.coinwashmanager2.model.MemberItem;
import com.example.coinwashmanager2.model.MemberJoinRequest;
import com.example.coinwashmanager2.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getMemberDate(long id) {
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void setMember(MemberJoinRequest request) {
        Member member = new Member.MemberBuilder(request).build();
        memberRepository.save(member);
    }

    public ListResult<MemberItem> getMembers() {
        List<Member> members = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberItem addItem = new MemberItem.MemberItemBuilder(member).build();
            result.add(addItem);
        });

        return  ListConvertService.settingResult(result);
    }

    public ListResult<MemberItem> getMembers(boolean isEnable) {
        List<Member> members = memberRepository.findAllByIsEnableOrderByIdDesc(isEnable);

        List<MemberItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberItem addItem = new MemberItem.MemberItemBuilder(member).build();
            result.add(addItem);
        });

            return  ListConvertService.settingResult(result);
    }

    public void putMemberWithdrawal(long id) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);

        if (!member.getIsEnable()) throw  new CNoMemberDataException();

        member.putWithdrawal();
        memberRepository.save(member);
    }


}
