package com.example.coinwashmanager2.service;

import com.example.coinwashmanager2.entity.Machine;
import com.example.coinwashmanager2.enums.MachineType;
import com.example.coinwashmanager2.exception.CMissingDataException;
import com.example.coinwashmanager2.model.*;
import com.example.coinwashmanager2.repository.MachineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MachineService {

    private final MachineRepository machineRepository;

    public void setMachine(MachineRequest request) {
        Machine machine = new Machine.MachineBuilder(request).build();
        machineRepository.save(machine);

    }

    public Machine getMachineDate(long id) {
        return machineRepository.findById(id).orElseThrow(ClassCastException::new);
    }

    public MachineDetail getMachine(long id) {
        Machine machine = machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new MachineDetail.MachineDetailBuilder(machine).build();


    }

    public ListResult<MachineItem> getMachines() {
        List<MachineItem> result = new LinkedList<>();

        List<Machine> machines = machineRepository.findAll();

//        for (Machine machine : machines) {
//            MachineItem machineItem = new MachineItem.MachineItemBuilder(machine).build();
//            result.add(machineItem);
//        }

        machines.forEach(machine -> {
            MachineItem machineItem = new MachineItem.MachineItemBuilder(machine).build();
            result.add(machineItem);
        });

        return ListConvertService.settingResult(result);
    }

    public ListResult<MachineItem> getMachines(MachineType machineType) {
        List<MachineItem> result = new LinkedList<>();

        List<Machine> machines = machineRepository.findAllByMachineTypeOrderByIdDesc(machineType);

        machines.forEach(machine -> {
            MachineItem machineItem = new MachineItem.MachineItemBuilder(machine).build();
            result.add(machineItem);
        });

        return ListConvertService.settingResult(result);

    }

    public void putMachineName(long id, @Valid MachineNameUpdateRequest request) {
        Machine machine = machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        machine.putDateName(request);

        machineRepository.save(machine);


    }
}
