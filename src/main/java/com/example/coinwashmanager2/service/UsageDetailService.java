package com.example.coinwashmanager2.service;

import com.example.coinwashmanager2.entity.Machine;
import com.example.coinwashmanager2.entity.Member;
import com.example.coinwashmanager2.entity.UsageDetails;
import com.example.coinwashmanager2.model.ListResult;
import com.example.coinwashmanager2.model.UsageDetailItem;
import com.example.coinwashmanager2.repository.UsageDetailsRepository;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UsageDetailService {
    private final UsageDetailsRepository usageDetailsRepository;

    public void setUsageDetails(Member member, Machine machine, LocalDateTime dateUsage) {
        UsageDetails usageDetails = new UsageDetails.UsageDetailsBuilder(machine, member, dateUsage).build();

        usageDetailsRepository.save(usageDetails);
    }

    public ListResult<UsageDetailItem> getUsageDetails(LocalDate dateStart, LocalDate dateEnd) { //시작일, 종료일
        LocalDateTime dateStartTime = LocalDateTime.of(
                dateStart.getYear(),
                dateStart.getMonthValue(),
                dateStart.getDayOfMonth(),
                0,
                0,
                0
        );

        LocalDateTime datEndTime = LocalDateTime.of(
                dateEnd.getYear(),
                dateEnd.getMonthValue(),
                dateEnd.getDayOfMonth(),
                23,
                59,
                59
        );
        List<UsageDetails> usageDetails = usageDetailsRepository.findAllByDateUsageGreaterThanEqualAndDateUsageLessThanEqualOrderByIdDesc(dateStartTime,datEndTime);

        List<UsageDetailItem> result = new LinkedList<>();

        usageDetails.forEach(usageDetails1 -> {
            UsageDetailItem addItem = new UsageDetailItem.UsageDetailItemBuilder(usageDetails1).build();
            result.add(addItem);
        });

        return  ListConvertService.settingResult(result);

    }



}
