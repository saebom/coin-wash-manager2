package com.example.coinwashmanager2.controller;

import com.example.coinwashmanager2.entity.Member;
import com.example.coinwashmanager2.model.CommonResult;
import com.example.coinwashmanager2.model.ListResult;
import com.example.coinwashmanager2.model.MemberItem;
import com.example.coinwashmanager2.model.MemberJoinRequest;
import com.example.coinwashmanager2.service.MemberService;
import com.example.coinwashmanager2.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "회원 관리 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {

    private final MemberService memberService;
    @ApiOperation(value = "회원 정보 등록하기")
    @PostMapping("/new")
    public CommonResult setMember(@RequestBody @Valid MemberJoinRequest request) {
        memberService.setMember(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 정보 리스트 가져오기")
    @GetMapping("/all")
    public ListResult<MemberItem> getMembers() {

        return ResponseService.getListResult(memberService.getMembers(),true);

    }

    @ApiOperation(value = "회원 정보 삭제하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "회원 시퀀스", required = true)
    })
    @DeleteMapping("/{Id}")
    public CommonResult delMember(@PathVariable long id) {
        memberService.putMemberWithdrawal(id);
        return ResponseService.getSuccessResult();
    }
}
