package com.example.coinwashmanager2.controller;

import com.example.coinwashmanager2.entity.Machine;
import com.example.coinwashmanager2.entity.Member;
import com.example.coinwashmanager2.model.CommonResult;
import com.example.coinwashmanager2.model.ListResult;
import com.example.coinwashmanager2.model.UsageDetailItem;
import com.example.coinwashmanager2.model.UsageDetailRequest;
import com.example.coinwashmanager2.service.MachineService;
import com.example.coinwashmanager2.service.MemberService;
import com.example.coinwashmanager2.service.ResponseService;
import com.example.coinwashmanager2.service.UsageDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "이용내역 관리 프로그램")
@RequiredArgsConstructor
@RequestMapping("/v1/usage-details")

public class UsageDetailController {

    private final UsageDetailService usageDetailService;

    private final MachineService machineService;

    private final MemberService memberService;

    @ApiOperation(value = "이용내역 등록하기")
    @PostMapping("/new")
    public CommonResult setUsageDetails(@RequestBody @Valid UsageDetailRequest request) {
        Member member = memberService.getMemberDate(request.getMemberId());
        Machine machine = machineService.getMachineDate(request.getMachineId());

        usageDetailService.setUsageDetails(member, machine , request.getDateUsage());

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "이용내역 리스트 가져오기")
    @GetMapping("/search")
    public ListResult<UsageDetailItem> usageDetails(
            @RequestParam(value = "dateStart") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateStart,
            @RequestParam(value = "dateEnd") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateEnd
    ) {
        return ResponseService.getListResult(usageDetailService.getUsageDetails(dateStart, dateEnd), true);
    }
}
