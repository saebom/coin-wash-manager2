package com.example.coinwashmanager2.controller;

import com.example.coinwashmanager2.enums.MachineType;
import com.example.coinwashmanager2.model.*;
import com.example.coinwashmanager2.service.MachineService;
import com.example.coinwashmanager2.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "기계 관리 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/machine")
public class MachineController {

    private final MachineService machineService;


    @ApiOperation(value = "기계 정보 등록하기")
    @PostMapping("/new")
    public CommonResult setMachine(@RequestBody @Valid MachineRequest request) {
        machineService.setMachine(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation("기계 정보 하나 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "기계 시퀀스" , required = true)
    })
    @GetMapping("/{id}")
    public SingleResult<MachineDetail> getMachines(@PathVariable long id) {
        return ResponseService.getSingleResult(machineService.getMachine(id));
    }

    @ApiOperation(value = "기계 정보 리스트 가져오기")
    @GetMapping("/search")

    public ListResult<MachineItem> getMachines(@RequestParam(value = "machineType",required = false )MachineType machineType) {
        if (machineType == null) {
            return ResponseService.getListResult(machineService.getMachines(),true );
        } else  {
            return ResponseService.getListResult(machineService.getMachines(machineType), true);
        }
    }
    @ApiOperation(value = "기계 정보 이름 수정하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "기계 시퀀스", required = true)
    })
    @PutMapping("/{id}")
    public CommonResult putMachineName(@PathVariable long id, @RequestBody @Valid MachineNameUpdateRequest request) {
        machineService.putMachineName(id, request);

        return ResponseService.getSuccessResult();
    }
}
