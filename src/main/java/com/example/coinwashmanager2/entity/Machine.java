package com.example.coinwashmanager2.entity;

import com.example.coinwashmanager2.enums.MachineType;
import com.example.coinwashmanager2.interfaces.CommonModelBuilder;
import com.example.coinwashmanager2.model.MachineNameUpdateRequest;
import com.example.coinwashmanager2.model.MachineRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)

public class Machine {

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private MachineType machineType;

    @Column(nullable = false, length = 20)
    private String machineName;

    @Column(nullable = false)
    private LocalDate datePurchase;

    private Double machinePrice;

    @Column(nullable = false)
    private Boolean isUsed;

    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putDateName(MachineNameUpdateRequest request) {
        this.machineName = request.getMachineName();
    }

    private Machine(MachineBuilder machineBuilder) {
        this.machineType = machineBuilder.machineType;
        this.machineName = machineBuilder.machineName;
        this.datePurchase = machineBuilder.datePurchase;
        this.isUsed = machineBuilder.isUsed;
        this.dateCreate = machineBuilder.dateCreate;
        this.dateUpdate = machineBuilder.dateUpdate;
    }

    public static class MachineBuilder implements CommonModelBuilder<Machine> {
        private final MachineType machineType;
        private final String machineName;
        private final LocalDate datePurchase;
        private final Boolean isUsed;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MachineBuilder(MachineRequest request) {
            this.machineType = request.getMachineType();
            this.machineName = request.getMachineName();
            this.datePurchase = request.getDatePurchase();
            this.isUsed = request.getIsUsed();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }


        @Override
        public Machine build() {
            return new Machine(this);
        }
    }
}
