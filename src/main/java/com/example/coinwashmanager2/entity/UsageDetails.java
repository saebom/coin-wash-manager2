package com.example.coinwashmanager2.entity;

import com.example.coinwashmanager2.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.hql.spi.id.cte.CteValuesListDeleteHandlerImpl;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "machine")
    private Machine machine;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false)
    private LocalDateTime dateUsage;

    private UsageDetails(UsageDetailsBuilder builder) {
        this.machine = builder.machine;
        this.member = builder.member;
        this.dateUsage = builder.dateUsage;
    }

    public static class UsageDetailsBuilder implements CommonModelBuilder<UsageDetails> {

        private final Machine machine;

        private final Member member;

        private final LocalDateTime dateUsage;

        public UsageDetailsBuilder(Machine machine, Member member,LocalDateTime dateUsage ) {
            this.machine = machine;
            this.member = member;
            this.dateUsage = dateUsage;
        }

        public UsageDetails build() {
            return new UsageDetails(this);
        }
    }
}
